package exercicio.jpa;

import javax.persistence.Column;

public class Estado {

	@Column(columnDefinition = "char(2)", nullable = false)
	private String sigla;

	@Column(columnDefinition = "varchar(40)", nullable = false)
	private String nome;

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
