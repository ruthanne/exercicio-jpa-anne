package exercicio.jpa;

public enum SituacaoVendedorEnum {

	ATIVO("ATV"), SUSPENSO("SPN");

	private String situacao;

	private SituacaoVendedorEnum(String valorSituacao) {
		situacao = valorSituacao;
	}

	public String getSituacao() {
		return situacao;
	}

	public static SituacaoVendedorEnum valueOfCodigo(String sit) {
		for (SituacaoVendedorEnum situacao : values()) {
			if (situacao.getSituacao().equalsIgnoreCase(sit)) {
				return situacao;
			}
		}
		throw new IllegalArgumentException();
	}

}
