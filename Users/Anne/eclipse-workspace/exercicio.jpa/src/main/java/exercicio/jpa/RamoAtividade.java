package exercicio.jpa;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class RamoAtividade {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	// Os valores ser�o gerados a partir de uma sequence. 
	// Caso n�o seja especificado um nome para a sequence, ser� utilizada 
	// uma sequence padr�o, a qual ser� global, para todas as entidades.
	@Column(nullable = false)
	private Integer id;

	@Column(columnDefinition = "varchar(40)", nullable = false)
	private String nome;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
