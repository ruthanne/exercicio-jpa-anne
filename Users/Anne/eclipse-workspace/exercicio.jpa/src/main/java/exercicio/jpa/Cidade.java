package exercicio.jpa;

import javax.persistence.Column;

public class Cidade {

	@Column(columnDefinition = "char(3)", nullable = false)
	private String sigla;

	@Column(columnDefinition = "varchar(40)", nullable = false)
	private String nome;
	
	@Column(columnDefinition = "char(2)", nullable = false)
	private Estado estado;

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

}
