package exercicio.jpa;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tab_vendedor")
public class Vendedor extends Funcionario {

	@Column(columnDefinition = "numeric(10,2)", nullable = false)
	private Double percentualComissao;
	
	@Column(columnDefinition = "char(3)", nullable = false)
	private SituacaoVendedorEnum situacao;
	
	private List<PessoaJuridica> clientes;
	
	public Vendedor(Double percentualComissao, SituacaoVendedorEnum situacao, List<PessoaJuridica> clientes) {
		super();
		this.percentualComissao = percentualComissao;
		this.situacao = situacao;
		this.clientes = clientes;
	}

	public Double getPercentualComissao() {
		return percentualComissao;
	}

	public void setPercentualComissao(Double percentualComissao) {
		this.percentualComissao = percentualComissao;
	}

	public SituacaoVendedorEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoVendedorEnum situacao) {
		this.situacao = situacao;
	}

	public List<PessoaJuridica> getClientes() {
		return clientes;
	}

	public void setClientes(List<PessoaJuridica> clientes) {
		this.clientes = clientes;
	}

	
}
