package exercicio.jpa;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Endereco {
	
	@Column(columnDefinition = "varchar(40)", nullable = false)
	private String logradouro;
	
	@Column(columnDefinition = "varchar(40)", nullable = false)
	private String bairro;
	
	@Column(columnDefinition = "char(30)", nullable = false)
	private Cidade cidade;

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
}
