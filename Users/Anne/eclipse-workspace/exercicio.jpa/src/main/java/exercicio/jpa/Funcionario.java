package exercicio.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "tab_funcionario", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "rg", "rg_orgao_expedidor", "rg_uf" }) })
public class Funcionario {

	@Id
	@Column(columnDefinition = "char(11)", nullable = false)
	private String cpf;

	@Column(columnDefinition = "varchar(40)", nullable = false)
	private String nome;

	@Column(columnDefinition = "char(12)")
	private String rg;

	@Column(name = "rg_orgao_expedidor", columnDefinition = "varchar(20)")
	private String rgOrgaoExpedidor;

	@Column(name = "rg_uf", columnDefinition = "char(2)")
	private String rgUf;

	@ElementCollection
	// It means that the collection is not a collection of entities,
	// but a collection of simple types. The elements are completely owned by the
	// containing entities: they're modified when the entity is modified, deleted
	// when the entity is deleted, etc.
	@Column(columnDefinition = "varchar(12)")
	private List<String> telefones;

	@Column(name = "data_nascimento", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;

	@Column(nullable = false)
	@Embedded // obter tabela com os campos das duas classes (Funcionario e Endereco)
	private Endereco endereco;

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getRgOrgaoExpedidor() {
		return rgOrgaoExpedidor;
	}

	public void setRgOrgaoExpedidor(String rgOrgaoExpedidor) {
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
	}

	public String getRgUf() {
		return rgUf;
	}

	public void setRgUf(String rgUf) {
		this.rgUf = rgUf;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

}
