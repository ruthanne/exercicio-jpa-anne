package exercicio.jpa;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tab_pessoa_juridica")
public class PessoaJuridica {

	@Column(columnDefinition = "char(11)", nullable = false)
	private String cnpj;
	
	@Column(columnDefinition = "varchar(40)", nullable = false)
	private String nome;
	
	private List<RamoAtividade> ramosAtividade;
	
	@Column(columnDefinition = "numeric(10,2)", nullable = false)
	private Double faturamento;
	
	private List<Vendedor> vendedores;

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<RamoAtividade> getRamosAtividade() {
		return ramosAtividade;
	}

	public void setRamosAtividade(List<RamoAtividade> ramosAtividade) {
		this.ramosAtividade = ramosAtividade;
	}

	public Double getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(Double faturamento) {
		this.faturamento = faturamento;
	}

	public List<Vendedor> getVendedores() {
		return vendedores;
	}

	public void setVendedores(List<Vendedor> vendedores) {
		this.vendedores = vendedores;
	}

}
