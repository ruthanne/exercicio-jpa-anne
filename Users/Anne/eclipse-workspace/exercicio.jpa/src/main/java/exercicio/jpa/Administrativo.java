package exercicio.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tab_administrativo")
public class Administrativo extends Funcionario{
	
	@Column(nullable = false)
	private Integer turno;
	
	public Administrativo(Integer turno) {
		super();
		this.turno = turno;
	}

	public Integer getTurno() {
		return turno;
	}

	public void setTurno(Integer turno) {
		this.turno = turno;
	}
	
}
